package com.mycompany.myproduct.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.mycompany.myproduct.entities.*;
import com.mycompany.myproduct.repositories.*;

@Controller
public class ProductController {
	
	@Autowired
	private ProductRepository repository;
	
	@GetMapping("/create")
	public String create(Model model) {
		
		model.addAttribute("product", new Product());
		
		return "productform"; // nama view
	}
	@PostMapping("/create")
	public String saveProduct(@ModelAttribute Product product, Model model) {
		// simpan ke database
		repository.save(product);		
		
		model.addAttribute("productid", product.getId());
		model.addAttribute("test1", 100);
		
		return "successview"; // nama view
	}
	@GetMapping("/list")
	public String viewAll(Model model) {
		
		model.addAttribute("list", repository.findAll());
		
		return "listview"; // nama view
	}
	@GetMapping("/product/{id}")
	public String viewDetail(@PathVariable(value = "id") Integer prodId, Model model) {
		
		Product prod = repository.findById(prodId).get();
		model.addAttribute("product", prod);
		
		
		
		return "detailview"; // nama view
	}

	@GetMapping("/product/edit/{id}")
	public String editProduct(@PathVariable(value = "id") Integer id, Model model) {
		Product p = repository.findById(id).get();
		model.addAttribute("product", p);

		return "editform";
	}

	@GetMapping("/product/delete/{id}")
	public String deleteProduct(@PathVariable(value = "id") Integer id, Model model) {
		Product p = repository.findById(id).get();

		if (p instanceof Product) {
			repository.delete(p);
		}

		model.addAttribute("list", repository.findAll());

		return "redirect:/list";
	}

	@PostMapping("/product/save/{id}")
	public String saveEditProduct(@PathVariable(value = "id") Integer id,@ModelAttribute Product product) {
		// simpan ke database
		Product oldProduct = repository.findById(id).get();
		oldProduct.setName(product.getName());
		oldProduct.setQuantity(product.getQuantity());

		repository.save(oldProduct);


		return "redirect:/list"; // nama view
	}


}


