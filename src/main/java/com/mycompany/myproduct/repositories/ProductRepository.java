package com.mycompany.myproduct.repositories;

import org.springframework.data.repository.CrudRepository;
import com.mycompany.myproduct.entities.*;

public interface ProductRepository extends CrudRepository<Product, Integer> {



}
